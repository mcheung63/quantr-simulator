package org.quantr.simulator;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JTextField;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

public class Comm {
	private static Logger logger = Logger.getLogger(Comm.class);

	static PoolingHttpClientConnectionManager cm;
	static RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(60000).setConnectionRequestTimeout(60000)
			.setStaleConnectionCheckEnabled(true).build();

	public static boolean testPostConnection(String url, HashMap<String, String> headers, HashMap<String, String> parameters) {
		try {
			PoolingHttpClientConnectionManager cm2 = null;
			RequestConfig defaultRequestConfig2 = RequestConfig.custom().setSocketTimeout(2000).setConnectTimeout(2000).setConnectionRequestTimeout(2000)
					.setStaleConnectionCheckEnabled(true).build();

			CloseableHttpClient httpClient = HttpClients.custom()
					.setSSLSocketFactory(new SSLConnectionSocketFactory(SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build()))
					.setDefaultRequestConfig(defaultRequestConfig2).setConnectionManager(cm2).build();
			try {
				HttpPost request = new HttpPost(url);
				//request.setHeader(HTTP.CONTENT_TYPE, "application/xml");
				if (headers != null) {
					Iterator<String> iterator = headers.keySet().iterator();
					while (iterator.hasNext()) {
						String key = iterator.next();
						request.setHeader(key, headers.get(key));
					}
				}
				if (parameters != null) {
					ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

					Iterator<String> iterator = parameters.keySet().iterator();
					while (iterator.hasNext()) {
						String key = iterator.next();
						postParameters.add(new BasicNameValuePair(key, parameters.get(key)));
					}
					request.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
				}

				HttpResponse response = httpClient.execute(request);
				HttpEntity responseEntity = response.getEntity();
				InputStream is = responseEntity.getContent();
				String myString = IOUtils.toString(is, "UTF-8");
				is.close();

				HttpResult httpResult = new HttpResult();
				httpResult.content = myString;
				httpResult.headers = response.getAllHeaders();
				return true;
			} catch (Exception ex) {
				logger.error(ex, ex);
			} finally {
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static HttpResult post(String url, HashMap<String, String> headers, HashMap<String, String> parameters) {
		return post(url, headers, parameters, false);
	}

	public static HttpResult post(String url, HashMap<String, String> headers, HashMap<String, String> parameters, boolean checkToken) {
		try {
			CloseableHttpClient httpClient = HttpClients.custom()
					.setSSLSocketFactory(new SSLConnectionSocketFactory(SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build()))
					.setDefaultRequestConfig(defaultRequestConfig).setConnectionManager(cm).build();

			try {
				HttpPost request = new HttpPost(url);
				//request.setHeader(HTTP.CONTENT_TYPE, "application/xml");
				if (headers != null) {
					Iterator<String> iterator = headers.keySet().iterator();
					while (iterator.hasNext()) {
						String key = iterator.next();
						request.setHeader(key, headers.get(key));
					}
				}
				if (parameters != null) {
					ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

					Iterator<String> iterator = parameters.keySet().iterator();
					while (iterator.hasNext()) {
						String key = iterator.next();
						postParameters.add(new BasicNameValuePair(key, parameters.get(key)));
					}
					request.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
				}

				HttpResponse response = httpClient.execute(request);
				HttpEntity responseEntity = response.getEntity();
				InputStream is = responseEntity.getContent();
				String myString = IOUtils.toString(is, "UTF-8");
				is.close();

				HttpResult httpResult = new HttpResult();
				httpResult.content = myString;
				httpResult.headers = response.getAllHeaders();
				return httpResult;
			} catch (Exception ex) {
				logger.error(ex, ex);
			} finally {
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception ex) {
			logger.error(ex, ex);
		}
		return null;
	}

	public static HttpResult put(String url, HashMap<String, String> headers, String entity) {
		if (entity == null) {
			logger.debug("put(), url=" + url + ", headers=" + headers);
		} else {
			logger.debug("put(), url=" + url + ", headers=" + headers + ", entity=" + entity.replaceAll("'", "\\\\'").replaceAll("\"", "\\\\\""));
		}

		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();

		try {
			HttpPut request = new HttpPut(url);
			Iterator<String> iterator = headers.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				request.setHeader(key, headers.get(key));
			}
			if (entity != null) {
				request.setEntity(new StringEntity(entity, "UTF-8"));
			}

			HttpResponse response = httpClient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			InputStream is = responseEntity.getContent();
			String myString = IOUtils.toString(is, "UTF-8");
			is.close();

			HttpResult httpResult = new HttpResult();
			httpResult.content = myString;
			httpResult.headers = response.getAllHeaders();
			return httpResult;
		} catch (Exception ex) {
			logger.error(ex, ex);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static HttpResult postBytes(String url, HashMap<String, String> headers, File file) {
		logger.debug("postBytes(), url=" + url + ", headers=" + headers + ", file=" + file.getName());
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();

		try {
			HttpPost request = new HttpPost(url);
			Iterator<String> iterator = headers.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				request.setHeader(key, headers.get(key));
			}
			// if (bytes != null) {
			request.setEntity(new FileEntity(file));
			// request.setEntity(new ByteArrayEntity(bytes));
			// }

			HttpResponse response = httpClient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			InputStream is = responseEntity.getContent();
			String myString = IOUtils.toString(is, "UTF-8");
			is.close();

			HttpResult httpResult = new HttpResult();
			httpResult.content = myString;
			httpResult.headers = response.getAllHeaders();
			return httpResult;
		} catch (Exception ex) {
			logger.error(ex, ex);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static HttpResult get(String url, HashMap<String, String> headers, boolean getContent) {
		logger.debug("get(), url=" + url + ", headers=" + headers);
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();

		try {
			HttpGet request = new HttpGet(url);
			Iterator<String> iterator = headers.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				request.setHeader(key, headers.get(key));
			}
			HttpResponse response = httpClient.execute(request);
			String myString = null;
			if (getContent) {
				HttpEntity responseEntity = response.getEntity();
				InputStream is = responseEntity.getContent();
				myString = IOUtils.toString(is, "UTF-8");
				is.close();
			}

			HttpResult httpResult = new HttpResult();
			httpResult.content = myString;
			httpResult.headers = response.getAllHeaders();
			return httpResult;
		} catch (Exception ex) {
			logger.error(ex, ex);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static HttpResult delete(String url, HashMap<String, String> headers) {
		logger.debug("delete(), url=" + url + ", headers=" + headers);
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();

		try {
			HttpDelete request = new HttpDelete(url);
			Iterator<String> iterator = headers.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				request.setHeader(key, headers.get(key));
			}
			HttpResponse response = httpClient.execute(request);
			HttpEntity responseEntity = response.getEntity();
			String myString = null;
			if (responseEntity != null) {
				InputStream is = responseEntity.getContent();
				myString = IOUtils.toString(is, "UTF-8");
				is.close();
			}

			HttpResult httpResult = new HttpResult();
			httpResult.content = myString;
			httpResult.headers = response.getAllHeaders();
			return httpResult;
		} catch (Exception ex) {
			logger.error(ex, ex);
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Boolean getBoolean(String url, HashMap<String, String> headers, HashMap<String, String> parameters) {
		try {
			HttpResult httpResult = Comm.post(url, headers, parameters);
			if (httpResult != null && httpResult.content != null) {
				return Boolean.parseBoolean(httpResult.content);
			} else {
				return null;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	public static <T> T post(String url, HashMap<String, String> headers, HashMap<String, String> parameters, com.google.common.reflect.TypeToken<T> token) {
		HttpResult httpResult = post(url, headers, parameters);
		//System.out.println(httpResult.content);
		if (httpResult != null && httpResult.content != null) {
			return new Gson().fromJson(httpResult.content, token.getType());
		} else {
			return null;
		}
	}

	public static boolean checkNumericField(JTextField textField) {
		if (textField.getText().trim().equals("")) {
			textField.putClientProperty("hightlightRed", true);
			textField.repaint();
			return false;
		}
		try {
			int x = Integer.parseInt(textField.getText());
		} catch (Exception ex) {
			textField.putClientProperty("hightlightRed", true);
			textField.repaint();
			return false;
		}
		return true;
	}
}
