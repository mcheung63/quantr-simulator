/*
 * Copyright (C) 2016 peter.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.quantr.simulator.gui;

import com.peterswing.CommonLib;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import org.quantr.simulator.Comm;
import org.quantr.simulator.Global;
import org.quantr.simulator.HttpResult;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author peter
 */
@Component
public class ChartLabel extends JLabel {

	int offsetX = 50;
	int offsetY = 50;
	static int step = 10;
	public int pointX = offsetX;
	public int pointY = offsetY;
	Color red = new Color(255, 0, 0, 100);
	public Vector<Point> points = new Vector<Point>();
	int hkIndex;

	@Override
	public void paint(Graphics g) {
		try {
			g.setColor(Color.white);
			g.fillRect(0, 0, getWidth(), getHeight());

			// draw grid lines
			int height;
			if (this.getIcon() != null) {
				height = getIcon().getIconHeight();
			} else {
				height = getHeight() - 100;
			}
			int width;
			if (this.getIcon() != null) {
				width = getIcon().getIconWidth();
			} else {
				width = getWidth();
			}
			int highest = Integer.parseInt(MainFrame.me.highestPointTextField.getText());
			int lowest = Integer.parseInt(MainFrame.me.lowestPointTextField.getText());
			int diff = highest - lowest;
			for (int y = 0, z = 0; y <= height; y += height / 10, z++) {
				g.setColor(Color.lightGray);
				g.drawLine(offsetX, y + offsetY, width + offsetX, y + offsetY);
				g.setColor(Color.black);
				g.drawString(String.valueOf(highest - (diff / 10 * z)), 10, y + 5 + offsetY);
			}

			if (this.getIcon() != null) {
				g.drawImage(((ImageIcon) getIcon()).getImage(), offsetX, offsetY, null);
				drawBall(g, pointX, pointY);
			} else {
				g.drawImage(drawChart(), 0, 0, null);
				drawBall(g, pointX, pointY);
			}

		} catch (Exception ex) {

		}
	}

	void drawBall(Graphics g, int x, int y) {
		g.setColor(red);
		g.fillOval(x - 25, y - 25, 50, 50);
		g.setColor(Color.red);
		g.fillOval(x - 5, y - 5, 10, 10);
		g.setColor(Color.blue);
		g.drawString(String.valueOf(hkIndex), x - 10, y - 25);
	}

	public Image drawChart() {
		BufferedImage bi = new BufferedImage(MainFrame.me.chartLabel.getWidth(), MainFrame.me.chartLabel.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.getGraphics();

		Point lastPoint = null;
		synchronized (MainFrame.me.chartLabel.points) {
			for (Point point : MainFrame.me.chartLabel.points) {
				if (lastPoint != null) {
//				g.setColor(Color.black);
//				g.drawLine(lastPoint.x, lastPoint.y, point.x, point.y);

					Graphics2D g2 = (Graphics2D) g;
					g2.setColor(Color.black);
					g2.setStroke(new BasicStroke(3));
					g2.draw(new Line2D.Float(lastPoint.x, lastPoint.y, point.x, point.y));
				}
				lastPoint = point;
			}
		}

		return new ImageIcon(bi).getImage();
	}

	@Scheduled(fixedRate = 10)
	public void run() {
		Icon icon = null;
		int checkColor;
		BufferedImage image;
		if (MainFrame.me.chartLabel.getIcon() == null) {
			BufferedImage bi = new BufferedImage(MainFrame.me.chartLabel.getWidth(), MainFrame.me.chartLabel.getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics g = bi.getGraphics();
			//g.setColor(Color.red);
			MainFrame.me.chartLabel.paint(g);
			icon = new ImageIcon(bi);
			checkColor = 0;
			image = CommonLib.toBufferedImage(drawChart());
		} else {
			image = new BufferedImage(MainFrame.me.chartLabel.getWidth(), MainFrame.me.chartLabel.getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics g = image.getGraphics();
			g.drawImage(CommonLib.toBufferedImage(((ImageIcon) MainFrame.me.chartLabel.getIcon())), offsetX, offsetY, null);
			//image = image.getSubimage(offsetX, offsetY, image.getWidth() - offsetX, image.getHeight() - offsetY);
			icon = MainFrame.me.chartLabel.getIcon();
			checkColor = 200;
		}
		if (icon != null && Global.simulationStart) {
			step = icon.getIconWidth() / ((int) MainFrame.me.timeSpinner.getValue() * 1000 / 10);
			if (step == 0) {
				step = 1;
			}

//			try {
//				File outputfile = new File("/Users/peter/Desktop/image.png");
//				ImageIO.write(image, "png", outputfile);
//				System.exit(1);
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
			for (int tempY = offsetY; tempY < image.getHeight(); tempY++) {
				try {
					Color color = new Color(image.getRGB(MainFrame.me.chartLabel.pointX, tempY), true);
					//System.out.println(MainFrame.me.chartLabel.pointX + "," + tempY + " = " + color.getAlpha() + ":" + color.getRed() + "/" + color.getGreen() + "/" + color.getBlue());
					if (color.getAlpha() == 255 && (color.getRed() + color.getGreen() + color.getBlue()) / 3 <= checkColor) {
						MainFrame.me.chartLabel.pointY = tempY;
						//System.out.println(MainFrame.me.chartLabel.pointX + "," + tempY + " = " + (color.getRed() + color.getGreen() + color.getBlue()) / 3);
						break;
					}
				} catch (Exception ex) {

				}
			}
			int highest = Integer.parseInt(MainFrame.me.highestPointTextField.getText());
			int lowest = Integer.parseInt(MainFrame.me.lowestPointTextField.getText());
			int diff = highest - lowest;
			int height = icon.getIconHeight();
			int hkIndex = ((height - MainFrame.me.chartLabel.pointY) * diff / height) + lowest;
			MainFrame.me.chartLabel.hkIndex = hkIndex;
			MainFrame.me.pointTextField.setText(String.valueOf(hkIndex));

			if (MainFrame.me.chartLabel.pointX - offsetX < icon.getIconWidth() - step) {
				MainFrame.me.chartLabel.pointX += step;
			} else {
				MainFrame.me.chartLabel.pointX = offsetX;
			}

			MainFrame.me.chartLabel.repaint();

			if (MainFrame.me.urlCheckBox.isSelected()) {
				HashMap<String, String> headers = new HashMap<String, String>();
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put("hkIndex", String.valueOf(hkIndex));
				HttpResult httpResult = Comm.post(MainFrame.me.urlTextField.getText(), headers, parameters);
			}
		}
	}
}
