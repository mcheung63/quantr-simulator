/*
 * Copyright (C) 2016 peter.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.quantr.simulator;

import java.awt.image.BufferedImage;

/**
 *
 * @author peter
 */
public class Global {

	public static boolean simulationStart = false;

	public static BufferedImage clearGrayPixel(BufferedImage img) {
//		for (int x = 0; x < img.getWidth(); x++) {
//			for (int y = 0; y < img.getHeight(); y++) {
//				int rgb = img.getRGB(x, y);
//				Color color = new Color(rgb);
//				if ((color.getRed() + color.getGreen() + color.getBlue()) > 20 && color.getRed() == color.getGreen() && color.getGreen() == color.getBlue()) {
//					img.setRGB(x, y, Color.white.getRGB());
//				}
//			}
//		}
		return img;
	}
}
