package org.quantr.simulator;

import com.peterswing.PropertyUtil;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.quantr.simulator.gui.MainFrame;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Simulator {
	
	public static CommandLine cmd;
	public static Logger logger = Logger.getLogger(Simulator.class);
	
	public static void main(String args[]) {
		
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		try {
			options.addOption("v", "version", false, "display version info");
			cmd = parser.parse(options, args);
		} catch (ParseException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		
		if (cmd.hasOption("version") || cmd.hasOption("v")) {
			System.out.println("version : " + PropertyUtil.getProperty("version"));
			System.out.println("");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar quantr-simulatorXXX.jar [OPTION]", options);
			System.exit(1);
		}
		
		MainFrame mainFrame = new MainFrame();
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
		
		ApplicationContext ctx = SpringApplication.run(Simulator.class, new String[]{"--spring.main.banner-mode=OFF"});
		
	}
	
}
