package org.quantr.simulator;

import java.util.HashMap;
import static org.quantr.simulator.Simulator.logger;
import org.quantr.simulator.gui.MainFrame;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UpAndDown {

	public static boolean start = false;
	int point = 9000;
	int pointInterval = 3;
	int lowestPoint = 8900;
	int highestPoint = 9100;
	boolean direction = true;

	@Scheduled(fixedRate = 500)
	public void upAndDown() {
		if (start) {
			logger.info(point);
			if (MainFrame.me.urlCheckBox.isSelected()) {
				HashMap<String, String> headers = new HashMap<String, String>();
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put("hkIndex", String.valueOf(point));
				HttpResult httpResult = Comm.post(MainFrame.me.urlTextField.getText(), headers, parameters);
			}
			if (direction) {
				if (point + pointInterval <= highestPoint) {
					point += pointInterval;
				} else {
					point -= pointInterval;
					direction = !direction;
				}
			} else if (point - pointInterval >= lowestPoint) {
				point -= pointInterval;
			} else {
				point += pointInterval;
				direction = !direction;
			}
		}
	}
}
